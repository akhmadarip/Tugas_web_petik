<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TblSiswaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tblsiswa-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'no_induk') ?>

    <?= $form->field($model, 'password') ?>

    <?= $form->field($model, 'nm_siswa') ?>

    <?= $form->field($model, 'alamat') ?>

    <?= $form->field($model, 'tempat_lahir') ?>

    <?php // echo $form->field($model, 'tanggal_lahir') ?>

    <?php // echo $form->field($model, 'jk') ?>

    <?php // echo $form->field($model, 'agama') ?>

    <?php // echo $form->field($model, 'foto') ?>

    <?php // echo $form->field($model, 'sekolah_asal') ?>

    <?php // echo $form->field($model, 'nm_ortu') ?>

    <?php // echo $form->field($model, 'pekerjaan') ?>

    <?php // echo $form->field($model, 'kd_kelas') ?>

    <?php // echo $form->field($model, 'level') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
