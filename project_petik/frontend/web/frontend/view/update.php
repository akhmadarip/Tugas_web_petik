<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\Tblsiswa */

$this->title = 'Update Tblsiswa: ' . $model->no_induk;
$this->params['breadcrumbs'][] = ['label' => 'Tblsiswas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->no_induk, 'url' => ['view', 'id' => $model->no_induk]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tblsiswa-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
