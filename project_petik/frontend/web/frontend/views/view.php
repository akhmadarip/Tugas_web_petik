<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\Tblsiswa */

$this->title = $model->no_induk;
$this->params['breadcrumbs'][] = ['label' => 'Tblsiswas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tblsiswa-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->no_induk], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->no_induk], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'no_induk',
            'password',
            'nm_siswa',
            'alamat',
            'tempat_lahir',
            'tanggal_lahir',
            'jk',
            'agama',
            'foto',
            'sekolah_asal',
            'nm_ortu',
            'pekerjaan',
            'kd_kelas',
            'level',
        ],
    ]) ?>

</div>
