<?php 
namespace frontend\controllers;
use yii\web\Controller;
use frontend\models\Biodata;


class BiodataController extends Controller {

	public function actionBiodata(){
		$biodata = new Biodata();

		$biodata->setNrp('0111101110');
		$biodata->setNama('Fajri');
		$biodata->setKelas('Mekkah');
		$biodata->setJurusan('Teknik Informatika');

		return $this->render('biodata', ['biodata'=>$biodata]);
	}
}


?>