<?php
namespace frontend\models;
use yii\base\Model;

//class biodata

class Biodata extends Model {
	//atribut 

	private $nrp;
	private $nama;
	private $kelas;
	private $jurusan;

	//method Biodata 
	public function setNrp($nrp){
		$this->nrp = $nrp;

	}
	public function getNrp(){
		return $this->nrp;
	}
	public function setNama($nama){
		$this->nama = $nama;

	}
	public function getNama(){
		return $this->nama;
	}
	public function setKelas($kelas){
		$this->kelas = $kelas;

	}
	public function getKelas(){
		return $this->kelas;
	}
	public function setJurusan($jurusan){
		$this->jurusan = $jurusan;
	}
	public function getJurusan(){
		return $this->jurusan;
	}

}

?>