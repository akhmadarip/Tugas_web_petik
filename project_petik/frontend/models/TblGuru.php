<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_guru".
 *
 * @property string $nip
 * @property string $password
 * @property string $nm_guru
 * @property string $alamat
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $foto
 * @property string $telpon
 * @property string $agama
 * @property string $jabatan
 * @property string $gol
 * @property string $tamatan
 * @property string $level
 */
class TblGuru extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_guru';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nip', 'password', 'nm_guru', 'alamat', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'foto', 'telpon', 'agama', 'jabatan', 'gol', 'tamatan'], 'required'],
            [['nip', 'password'], 'string', 'max' => 20],
            [['nm_guru', 'foto', 'jabatan'], 'string', 'max' => 40],
            [['alamat', 'tamatan'], 'string', 'max' => 150],
            [['tempat_lahir'], 'string', 'max' => 30],
            [['tanggal_lahir'], 'string', 'max' => 13],
            [['jenis_kelamin', 'agama', 'level'], 'string', 'max' => 10],
            [['telpon'], 'string', 'max' => 12],
            [['gol'], 'string', 'max' => 7],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'nip' => 'Nip',
            'password' => 'Password',
            'nm_guru' => 'Nm Guru',
            'alamat' => 'Alamat',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'foto' => 'Foto',
            'telpon' => 'Telpon',
            'agama' => 'Agama',
            'jabatan' => 'Jabatan',
            'gol' => 'Gol',
            'tamatan' => 'Tamatan',
            'level' => 'Level',
        ];
    }

    /**
     * @inheritdoc
     * @return TblGuruQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TblGuruQuery(get_called_class());
    }
}
