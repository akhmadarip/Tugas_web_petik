<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "tbl_siswa".
 *
 * @property string $no_induk
 * @property string $password
 * @property string $nm_siswa
 * @property string $alamat
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $jk
 * @property string $agama
 * @property string $foto
 * @property string $sekolah_asal
 * @property string $nm_ortu
 * @property string $pekerjaan
 * @property string $kd_kelas
 * @property string $level
 */
class Tblsiswa extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_siswa';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_induk', 'password', 'nm_siswa', 'alamat', 'tempat_lahir', 'tanggal_lahir', 'jk', 'agama', 'sekolah_asal', 'nm_ortu', 'pekerjaan', 'kd_kelas'], 'required'],
            [['no_induk', 'tanggal_lahir', 'jk', 'agama', 'level'], 'string', 'max' => 10],
            [['password', 'tempat_lahir'], 'string', 'max' => 15],
            [['nm_siswa', 'sekolah_asal', 'nm_ortu', 'pekerjaan'], 'string', 'max' => 20],
            [['foto'], 'file', 'extensions'=>'jpeg,png,jpg,gif'],
            [['alamat'], 'string', 'max' => 30],
            [['kd_kelas'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'no_induk' => 'No Induk',
            'password' => 'Password',
            'nm_siswa' => 'Nm Siswa',
            'alamat' => 'Alamat',
            'tempat_lahir' => 'Tempat Lahir',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jk' => 'Jk',
            'agama' => 'Agama',
            'foto' => 'Foto',
            'sekolah_asal' => 'Sekolah Asal',
            'nm_ortu' => 'Nm Ortu',
            'pekerjaan' => 'Pekerjaan',
            'kd_kelas' => 'Kd Kelas',
            'level' => 'Level',
        ];
    }

    /**
     * @inheritdoc
     * @return TblQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TblQuery(get_called_class());
    }
}
