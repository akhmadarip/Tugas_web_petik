<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[TblGuru]].
 *
 * @see TblGuru
 */
class TblGuruQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return TblGuru[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return TblGuru|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
