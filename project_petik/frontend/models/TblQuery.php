<?php

namespace frontend\models;

/**
 * This is the ActiveQuery class for [[Tblsiswa]].
 *
 * @see Tblsiswa
 */
class TblQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Tblsiswa[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Tblsiswa|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
