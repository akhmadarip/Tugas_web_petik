<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use frontend\models\Tblsiswa;

/**
 * TblSiswaSearch represents the model behind the search form about `frontend\models\Tblsiswa`.
 */
class TblSiswaSearch extends Tblsiswa
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['no_induk', 'password', 'nm_siswa', 'alamat', 'tempat_lahir', 'tanggal_lahir', 'jk', 'agama', 'foto', 'sekolah_asal', 'nm_ortu', 'pekerjaan', 'kd_kelas', 'level'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tblsiswa::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'no_induk', $this->no_induk])
            ->andFilterWhere(['like', 'password', $this->password])
            ->andFilterWhere(['like', 'nm_siswa', $this->nm_siswa])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['like', 'tanggal_lahir', $this->tanggal_lahir])
            ->andFilterWhere(['like', 'jk', $this->jk])
            ->andFilterWhere(['like', 'agama', $this->agama])
            ->andFilterWhere(['like', 'foto', $this->foto])
            ->andFilterWhere(['like', 'sekolah_asal', $this->sekolah_asal])
            ->andFilterWhere(['like', 'nm_ortu', $this->nm_ortu])
            ->andFilterWhere(['like', 'pekerjaan', $this->pekerjaan])
            ->andFilterWhere(['like', 'kd_kelas', $this->kd_kelas])
            ->andFilterWhere(['like', 'level', $this->level]);

        return $dataProvider;
    }
}
