<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TblSiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tblsiswas';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tblsiswa-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tblsiswa', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'no_induk',
            'password',
            'nm_siswa',
            'alamat',
            'tempat_lahir',
            // 'tanggal_lahir',
            // 'jk',
            // 'agama',
            'foto',
            // 'sekolah_asal',
            // 'nm_ortu',
            // 'pekerjaan',
            // 'kd_kelas',
            // 'level',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
