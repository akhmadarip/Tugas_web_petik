<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Tblsiswa */

$this->title = 'Create Tblsiswa';
$this->params['breadcrumbs'][] = ['label' => 'Tblsiswas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tblsiswa-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
